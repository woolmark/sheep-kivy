# -*- coding:utf-8 -*-
from kivy.app import App
from kivy.clock import Clock
from kivy.core.text import LabelBase, DEFAULT_FONT
from kivy.core.window import Window
from kivy.properties import NumericProperty
from kivy.resources import resource_add_path
from kivy.uix.image import Image
from kivy.uix.floatlayout import FloatLayout
import random

class ASheep():
    SHEEP00_IMAGE_NAME = "sheep00.png"
    SHEEP01_IMAGE_NAME = "sheep01.png"
    SHEEP00_IMAGE_WIDTH = 17
    SHEEP00_IMAGE_HEIGT = 12

    def __init__(self):
        self.init()
        self.set_images()

    def init(self):
        self.image = None
        self.is_running = False
        self.x = 0
        self.y = 0
        self.progress_on_jump = 0
        self.x_on_jump = 0
        self.image = None
        self.stretch_leg = False

    def set_images(self):
        self.images = [Image(source=self.SHEEP00_IMAGE_NAME), Image(source=self.SHEEP01_IMAGE_NAME)]


class Sheep(FloatLayout):

    DEFAULT_WIDTH = 120
    DEFAULT_HEIGHT = 120

    FPS = 10

    sleep_time = FPS/100.0
    MAX_SHEEP_NUMBER = 100

    COUNTER_POSITION_X = 4
    COUNTER_POSITION_Y = 4

    X_MOVING_DIST = 5
    Y_MOVING_DIST = 3

    TOP_ON_JUMP = 5

    GROUND_HEIGHT_RATIO = 0.9

    sheep_flock = []

    sheep_count = NumericProperty(0)

    ground_height = 0

    gate_is_widely_open = False

    running = False

    fence_bitmap = None

    FENCE_IMAGE_NAME = "fence.png"
    FENCE_IMAGE = None
    FENCE_IMAGE_WIDTH = NumericProperty(52)
    FENCE_IMAGE_HEIGHT = NumericProperty(78)

    sheep_bitmap = []

    def __init__(self, **kwargs):
        super(Sheep, self).__init__(**kwargs)

        Window.size = (self.DEFAULT_WIDTH, self.DEFAULT_HEIGHT)

        resource_add_path("/Library/Fonts/")
        LabelBase.register(DEFAULT_FONT, "Yu Gothic Medium.otf")

        self.pos=(0, 0)

        self.ground_height=int(self.FENCE_IMAGE_HEIGHT * self.GROUND_HEIGHT_RATIO)

        # initialize sheep_flock
        for i in range(0, self.MAX_SHEEP_NUMBER):
            self.sheep_flock.append(ASheep())

        # Add first sheep
        self.send_out_sheep(self.sheep_flock[0])

        Clock.schedule_interval(self.update, self.sleep_time)

    def update(self, delta):
        self.calc()
        self.draw()

    def remove_sheep(self, asheep):
        asheep.init()

    def send_out_sheep(self, asheep):
        asheep.is_running = True
        # asheep.x = self.DEFAULT_WIDTH + self.SHEEP00_IMAGE_WIDTH
        asheep.x = self.DEFAULT_WIDTH
        asheep.y = random.randrange(self.ground_height)
        asheep.x_on_jump = self.calc_jump_x(asheep.y)

    def calc_jump_x(self, y):
        return y * self.FENCE_IMAGE_WIDTH / self.FENCE_IMAGE_HEIGHT + (self.DEFAULT_WIDTH - self.FENCE_IMAGE_WIDTH) / 2

    def calc(self):
        # Send out a sheep
        if self.gate_is_widely_open:
            for asheep in self.sheep_flock:
                if not asheep.is_running:
                    self.send_out_sheep(asheep)
                    break

        # Update status for each sheep
        for asheep in self.sheep_flock:
            if not asheep.is_running:
                continue

            # Run
            asheep.x -= self.X_MOVING_DIST

            # start to jump when sheep reach to jump point
            if not asheep.progress_on_jump and asheep.x_on_jump <= asheep.x and asheep.x < asheep.x_on_jump + self.X_MOVING_DIST:
                asheep.progress_on_jump += 1

            # behavior during jump
            if asheep.progress_on_jump > 0:
                if asheep.progress_on_jump < self.TOP_ON_JUMP:
                    asheep.y += self.Y_MOVING_DIST
                    asheep.progress_on_jump += 1
                elif asheep.progress_on_jump < self.TOP_ON_JUMP * 2:
                    asheep.y -= self.Y_MOVING_DIST
                    asheep.progress_on_jump += 1
                else:
                    asheep.progress_on_jump = 0

            # count up
            if asheep.progress_on_jump == self.TOP_ON_JUMP:
                self.sheep_count += 1

            # go away and send out a sheep if there is no sheep on run
            if asheep.x < -1 * asheep.SHEEP00_IMAGE_WIDTH:
                some_sheep_is_running = False
                self.remove_sheep(asheep)
                for asheep in self.sheep_flock:
                    if asheep.is_running:
                        some_sheep_is_running = True
                        break
                if not some_sheep_is_running:
                    self.send_out_sheep(asheep)

    def draw(self):
        self.clear_widgets()
        self.add_widget(self.counter)
        # Draw the sheep
        for asheep in self.sheep_flock:
            if asheep.is_running:
                if asheep.image is not None:
                    self.remove_widget(asheep.image)
                    asheep.image = None
                if asheep.progress_on_jump > 0:
                    asheep.image = asheep.images[True]
                else:
                    asheep.image = asheep.images[asheep.stretch_leg]
                asheep.image.pos = (asheep.x - self.DEFAULT_WIDTH/2, asheep.y  - self.DEFAULT_HEIGHT/2 )

                self.add_widget(asheep.image)

                # Flip animation
                asheep.stretch_leg = not asheep.stretch_leg

    def on_touch_down(self, event):
        self.gate_is_widely_open = True

    def on_touch_up(self, event):
        self.gate_is_widely_open = False


class SheepApp(App):
    def build(self):
        return Sheep()


if __name__ == '__main__':
    SheepApp().run()
