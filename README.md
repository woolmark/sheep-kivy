# Setup to run

Install following packages

 - Kivy 1.8.0 with Python 2.7.8

# Run the sheep

Launch `main.py` on terminal or command prompt

    % kivy main.py

# License

WTFPL
